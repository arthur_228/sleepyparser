# sleepyparser

## What is it and how does it work?
It's a lil python script that'll help you to take auto generated subtitles, ready to be sent to your davinci projects. Was struggling to find a good voice-to-text recognition that'll do it quick and with no paywalls, so figured a solution for myself, and for other small editors in need.<br />
Warning! This script uses third party service that provides 30 minutes of subtitles generation, that can be accessed/modified/copied in its web ui, but doesn't have built-in exporting with free plan. This script is a workaround that extracts subtitles from html page and formats them properly into `.srt`.

After initial setup, it takes me just around **3 minutes** to get from subtitleless video/edit to full on ready-to-post result.

## Installation/Initial Setup
### 1 Python
This script is a Python code, so first you need Python on your PC if you don't have it, thus:

>1.1 Download it from official site
 [https://www.python.org/downloads/](https://www.python.org/downloads/)

>1.2 Launch installer and follow prompts: you can pick "Install Now" if you don't want to think extra time, everything should work regardless


### 2 Script itself
Now download actual script from here, for that:
>2.1 Click on `sleepyparser.py` right above and hit lil download button (on top left). You can check code itself here as well if you're curious (recommend to choose accessible save location)

### 3* Snap Captions (DaVinci Plugin)

With DaVinci you'll get your subtitles in `Subtitle Track` not in fancy `Text+`. If you need `Text+` there are multiple ways to convert them, that's one I feel good about:
>3.1 Follow instructions here [https://mediable.notion.site/Snap-Captions-Install-Guide-f39ead46635148d9bd4c4e1052c43d19](https://mediable.notion.site/Snap-Captions-Install-Guide-f39ead46635148d9bd4c4e1052c43d19)

## Using the script

> 1 Script requires html file of [veed.io](https://www.veed.io/) project *(online video editor)*, so go there *(I use chrome your experience might differ)*

> 2 `Start for free` > Upload your Video > Go to `Subtitles` tab > Hit `AUTO Subtitle` > `Create Subtitles` <br />
*\* That's where you can go to next step, but I suggest to go to `Options` > `Auto Format` > choose `1 Lines` for fine tuning text (generator do make some mistakes) - that determines how much your subtitles will be chopped, likely want less text bunched for future viewing experience (or maybe not?)*


> 3 **Zoom out!** Do so that your browser can see all subtitles without scrolling > `Save Page as...` `Webpage, Complete` (default option in Chrome), to do that you can click on `Address Bar` and hit `Ctrl+S` > Choose folder where you saved your `sleepyparser.py` > `Save`

> 4 Run `sleepyparser.py` *(usually by double clicking it, otherwise open with Python and might as well set py files to associate with Python while you're at it)*<br />
Make sure you have `VEED CREATE _ Edit.html` laying nearby<br />
You can also change settings of the script if you open it with notepad or smth, more settings will be added eventually (and maybe will transport all that into separate file who knows?)

> 5 You've got it! You'll find `.srt` file nearby that you can just drag to `DaVinci Timeline`

> 6 The last part is to make them into formatted `Text+`. that's where you use the `Snap Captions` Plugin (can revisit intructions from 3.1 of Installation)


## Have questions or issues?
Feel free to ask, quickest respond time @discord, will be happy to improve your user experience!