######DEFINES
###FILES
HTML_FILE = "VEED CREATE _ Edit.html"
OUTPUT_FILE = "sub_file.srt"
###SRT SETTINGS
#COMING SOON (possibly splitting in two lines and potentially clean up, adding 0:0 entry for aligning)
###CHANGE IN CASE OF VEED.IO UPDATES
LINE_IDENTIFIER = "--subtitles-current-percentage-diff"
TIMECODE_BEACON = "@editor/subtitle-row/timing-text"
TEXT_BEACON = "<span data-text=\"true\">"
START_BEACON = "subtitles-editor"
END_BEACON = "Add New Subtitles Line"
######DEFINES

#returns position of next timecode in given text
def get_next_timecode_pos(cur_str):
	timecode_seeker = cur_str.find(TIMECODE_BEACON)
	if timecode_seeker != -1:
		timecode_seeker = cur_str.find(">",timecode_seeker) + 1
	return timecode_seeker

#returns timecode text in given text, at given position
def get_timecode_by_pos(cur_str, index):
	return cur_str[index:index+7]

#returns all subtitle text in given text
def get_text(cur_str):
	result=""
	text_seeker = cur_str.find(TEXT_BEACON)
	while text_seeker != -1:
		text_start_pos = text_seeker+len(TEXT_BEACON)
		text_end_pos = cur_str.find("<",text_start_pos)
		result += cur_str[text_start_pos:text_end_pos]
		text_seeker = cur_str.find(TEXT_BEACON,text_end_pos)
	return result

#subtitle handler class
class sub_item(object):
	def __init__(self, index, start_time, end_time, text):
		self.index = str(index)
		self.start_time = f"00:{start_time.replace('.',',')}00"
		self.end_time = f"00:{end_time.replace('.',',')}00"
		self.text = text
	def format(self):
		return(f"{self.index}\n{self.start_time} --> {self.end_time}\n{self.text}\n\n")		


#INITIALIZATION
sauce = open(HTML_FILE, "r", encoding='utf-8')
sub_counter = 0
t0 = 0

#PARSER+PRINTER
#--locating the line+getting important part
while line := sauce.readline():
	if line[:250].find(LINE_IDENTIFIER) != -1:
		break
sauce.close()

#--creating srt file
sub_file = open(OUTPUT_FILE, "w")

#--setting global borders
block_start = line.find(START_BEACON)
block_end = line.find(END_BEACON, block_start)
subline = line[block_start:block_end]

#--cycle_search:look for next timecodes > gather text before them > move subline start
while True:
	t0 = get_next_timecode_pos(subline)
	if t0 != -1:
		sub_counter += 1
		t1 = get_next_timecode_pos(subline[t0:])
		sub_text = get_text(subline[:t0])
		t0_code = get_timecode_by_pos(subline, t0)
		t1_code = get_timecode_by_pos(subline[t0:], t1)
		sub = sub_item(sub_counter, t0_code, t1_code, sub_text)
#--writing sub_item to file
		sub_file.write(sub.format())
		subline = subline[t0+t1+7:]
	else:
		break
#EXIT
sub_file.close()
quit()